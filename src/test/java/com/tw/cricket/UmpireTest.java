package com.tw.cricket;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UmpireTest {

    @Test
    void shouldDeclareBatsmanAsNotOutIfBatsmanScoreIsNotEqualToBowlerScore() {
        int batsmanRuns = 5;
        int bowlerRuns = 6;
        Umpire umpire = new Umpire();
        Batsman batsman = new Batsman(new NormalBatsmanBehaviour(), BatsmanType.NORMAL);

        assertFalse(umpire.decideBatsmanOut(batsmanRuns, bowlerRuns, batsman.getDismissalStrategy()));
    }

    @Test
    void shouldDeclareBatsmanAsOutIfBatsmanScoreIsEqualToBowlerScore() {
        int batsmanRuns = 5;
        int bowlerRuns = 5;
        Umpire umpire = new Umpire();
        Batsman batsman = new Batsman(new NormalBatsmanBehaviour(), BatsmanType.NORMAL);

        assertTrue(umpire.decideBatsmanOut(batsmanRuns, bowlerRuns, batsman.getDismissalStrategy()));
    }

    @Test
    void shouldDeclareBatsmanAsNotOutIfTailEnderBatsmanScoreIsNotEqualToBowlerScore() {
        int batsmanRuns = 5;
        int bowlerRuns = 6;
        Umpire umpire = new Umpire();
        Batsman batsman = new Batsman(new NormalBatsmanBehaviour(), BatsmanType.TAIL_ENDER);

        assertFalse(umpire.decideBatsmanOut(batsmanRuns, bowlerRuns, batsman.getDismissalStrategy()));
    }

    @Test
    void shouldDeclareBatsmanAsOutIfTailEnderBatsmanScoreIsEqualToBowlerScore() {
        int batsmanRuns = 5;
        int bowlerRuns = 5;
        Umpire umpire = new Umpire();
        Batsman batsman = new Batsman(new NormalBatsmanBehaviour(), BatsmanType.TAIL_ENDER);

        assertTrue(umpire.decideBatsmanOut(batsmanRuns, bowlerRuns, batsman.getDismissalStrategy()));
    }

    @Test
    void shouldDeclareBatsmanAsOutIfTailEnderBatsmanScoreAndBowlerScoreIsEvenNumber() {
        int batsmanRuns = 6;
        int bowlerRuns = 4;
        Umpire umpire = new Umpire();
        Batsman batsman = new Batsman(new NormalBatsmanBehaviour(), BatsmanType.TAIL_ENDER);

        assertTrue(umpire.decideBatsmanOut(batsmanRuns, bowlerRuns, batsman.getDismissalStrategy()));
    }

    @Test
    void shouldDeclareBatsmanAsOutIfTailEnderBatsmanScoreAndBowlerScoreIsOddNumber() {
        int batsmanRuns = 1;
        int bowlerRuns = 3;
        Umpire umpire = new Umpire();
        Batsman batsman = new Batsman(new NormalBatsmanBehaviour(), BatsmanType.TAIL_ENDER);

        assertTrue(umpire.decideBatsmanOut(batsmanRuns, bowlerRuns, batsman.getDismissalStrategy()));
    }
}