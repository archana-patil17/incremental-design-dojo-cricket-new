package com.tw.cricket;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.isOneOf;

public class BatsmanTest {
    @Test
    void shouldBeAbleToScoreRunsBetweenZeroAndSix() {
        Batsman batsman = new Batsman(new NormalBatsmanBehaviour(),BatsmanType.NORMAL);

        int runs = batsman.bat();

        assertThat(runs, isOneOf(0, 1, 2, 3, 4, 5, 6));
        assertThat(batsman.getDismissalStrategy(), instanceOf(NormalBatsManDismissalStrategy.class));
    }

    @Test
    void shouldBeAbleToScoreRunsBetweenZeroAndFourAndSix() {
        Batsman batsman = new Batsman(new HittingBatsmanBehaviour(),BatsmanType.HIT_MAN);

        int runs = batsman.bat();

        assertThat(runs, isOneOf(0, 4, 6));
        assertThat(batsman.getDismissalStrategy(), instanceOf(NormalBatsManDismissalStrategy.class));
    }

    @Test
    void shouldBeAbleToScoreRunsBetweenZeroAndOneAndTwoAndThreeAndFive() {
        Batsman batsman = new Batsman(new DefensiveBatsmanBehaviour(),BatsmanType.DEFENSIVE);

        int runs = batsman.bat();

        assertThat(runs, isOneOf(0, 1, 2, 3, 5));
        assertThat(batsman.getDismissalStrategy(), instanceOf(NormalBatsManDismissalStrategy.class));
    }

    @Test
    void shouldBeAbleToScoreRunsBetweenZeroAndSixForTailEnderBastMan() {
        Batsman batsman = new Batsman(new NormalBatsmanBehaviour(),BatsmanType.TAIL_ENDER);

        int runs = batsman.bat();

        assertThat(runs, isOneOf(0, 1, 2, 3, 4, 5, 6));
        assertThat(batsman.getDismissalStrategy(), instanceOf(TailEnderBatsmanDismissalStrategy.class));
    }
}
