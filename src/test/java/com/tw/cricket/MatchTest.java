package com.tw.cricket;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class MatchTest {

    @Test
    void shouldWonTheMatchIfBatsmanAchievedTheTarget() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(12, 1, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(5, 2, 3, 2);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(1, 1, 2, 1);

        match.play();

        verify(bowler, times(4)).bowl();
        verify(batsman, times(4)).bat();
        assertTrue(match.targetAchieved());
    }

    @Test
    void shouldLoseTheMatchIfBatsmanFailedToAchievedTheTarget() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(12, 1, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(5, 0, 1, 2, 1, 2);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(1, 1, 2, 1, 2, 1);

        match.play();

        verify(bowler, times(6)).bowl();
        verify(batsman, times(6)).bat();
        assertFalse(match.targetAchieved());
    }

    @Test
    void batsmanShouldLoseTheMatchWhenBatsmanAndBowlerScoresAreEqual() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(12, 1, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(5, 0, 1, 2, 1, 3);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(5);

        match.play();

        assertFalse(match.targetAchieved());
    }


    @Test
    void shouldWonTheMatchIfAttackingBatsmanAchievedTheTarget() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(20, 2, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(6, 0, 4, 6, 4);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(1, 1, 1, 4, 3);

        match.play();

        assertTrue(match.targetAchieved());
    }

    @Test
    void shouldLoseTheMatchIfAttackingBatsmanDoNotAchievedTheTarget() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(20, 2, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(0, 6, 4);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(1, 4, 4);

        match.play();

        assertFalse(match.targetAchieved());
    }

    @Test
    void shouldWonTheMatchIfDefensiveBatsmanAchievedTheTarget() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(10, 1, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(1, 2, 2, 3, 2);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(0, 1, 1, 4, 3);

        match.play();

        assertTrue(match.targetAchieved());
    }

    @Test
    void shouldLoseTheMatchIfDefensiveBatsmanDoNotAchievedTheTarget() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(20, 2, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(0, 5, 4);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(1, 4, 4);

        match.play();

        assertFalse(match.targetAchieved());
    }

    @Test
    void shouldWonTheMatchIfHitterBatsmanPlayedAgainstPartTimeBowlerAndAchievedTheTarget() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(12, 1, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(6, 4, 6);
        when(bowler.canTakeWicket()).thenReturn(false);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(1, 4, 4);

        match.play();

        assertTrue(match.targetAchieved());
    }

    @Test
    void shouldLoseTheMatchIfHitterBatsmanPlayedAgainstPartTimeBowlerAndDoNotAchievedTheTarget() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(12, 1, batsman, bowler, umpire);
        when(batsman.bat()).thenReturn(6, 4, 1);
        when(bowler.canTakeWicket()).thenReturn(false);
        when(batsman.getDismissalStrategy()).thenReturn(new NormalBatsManDismissalStrategy());
        when(bowler.bowl()).thenReturn(1, 4, 4);

        match.play();

        assertFalse(match.targetAchieved());
    }

    @Test
    void shouldLoseTheMatchIfTailEnderBatsmanPlayedAgainstNormalBowlerAndGetsOutWithEvenNumberScore() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(20, 1, batsman, bowler, umpire);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new TailEnderBatsmanDismissalStrategy());
        when(batsman.bat()).thenReturn(0, 6);
        when(bowler.bowl()).thenReturn(6, 4);

        match.play();

        assertFalse(match.targetAchieved());
    }

    @Test
    void shouldLoseTheMatchIfTailEnderBatsmanPlayedAgainstNormalBowlerAndGetsOutWithOddNumberScore() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(20, 1, batsman, bowler, umpire);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new TailEnderBatsmanDismissalStrategy());
        when(batsman.bat()).thenReturn(0, 3);
        when(bowler.bowl()).thenReturn(6, 5);

        match.play();

        assertFalse(match.targetAchieved());
    }

    @Test
    void shouldLoseTheMatchIfTailEnderBatsmanPlayedAgainstNormalBowlerAndGetsOutWithEqualNumberScore() {
        Batsman batsman = mock(Batsman.class);
        Bowler bowler = mock(Bowler.class);
        Umpire umpire = new Umpire();
        Match match = new Match(20, 1, batsman, bowler, umpire);
        when(bowler.canTakeWicket()).thenReturn(true);
        when(batsman.getDismissalStrategy()).thenReturn(new TailEnderBatsmanDismissalStrategy());
        when(batsman.bat()).thenReturn(0, 5);
        when(bowler.bowl()).thenReturn(1, 5);

        match.play();

        assertFalse(match.targetAchieved());
    }
}
