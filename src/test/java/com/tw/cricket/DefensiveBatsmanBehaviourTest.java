package com.tw.cricket;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isOneOf;

class DefensiveBatsmanBehaviourTest {
    @Test
    void shouldBeAbleToScoreRunsBetweenZeroAndOneAndTwoAndThreeAndFive() {
        DefensiveBatsmanBehaviour batsman = new DefensiveBatsmanBehaviour();

        int runs = batsman.bat();

        assertThat(runs, isOneOf(0, 1, 2, 3, 5));
    }
}