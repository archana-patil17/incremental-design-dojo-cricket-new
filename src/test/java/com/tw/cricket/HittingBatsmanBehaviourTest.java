package com.tw.cricket;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isOneOf;

class HittingBatsmanBehaviourTest {
    @Test
    void shouldBeAbleToScoreRunsBetweenZeroAndFourAndSix() {
        HittingBatsmanBehaviour batsman = new HittingBatsmanBehaviour();

        int runs = batsman.bat();

        assertThat(runs, isOneOf(0, 4, 6));
    }
}