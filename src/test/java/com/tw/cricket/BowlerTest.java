package com.tw.cricket;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isOneOf;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BowlerTest {
    @Test
    void shouldBeAbleToScoreRunsBetweenZeroAndSixAndNotTakeWicket() {
        Bowler bowler = new Bowler(BowlerType.NORMAL);

        int runs = bowler.bowl();

        assertThat(runs, isOneOf(0, 1, 2, 3, 4, 5, 6));
        assertTrue(bowler.canTakeWicket());
    }

    @Test
    void shouldBeAbleToScoreRunsBetweenZeroAndSixAndTakeWicket() {
        Bowler bowler = new Bowler(BowlerType.PART_TIME);

        int runs = bowler.bowl();

        assertThat(runs, isOneOf(0, 1, 2, 3, 4, 5, 6));
        assertFalse(bowler.canTakeWicket());
    }
}