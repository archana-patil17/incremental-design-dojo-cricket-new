package com.tw.cricket;

public class Match {
    public static final int BALLS_PER_OVER = 6;
    private final int targetScore;
    private final int overs;
    private final Batsman batsman;
    private final Bowler bowler;
    private final Umpire umpire;
    private int totalScore;

    public Match(int targetScore, int overs, Batsman batsman, Bowler bowler, Umpire umpire) {
        this.targetScore = targetScore;
        this.overs = overs;
        this.batsman = batsman;
        this.bowler = bowler;
        this.umpire = umpire;
    }

    public void play() {
        int balls = overs * BALLS_PER_OVER;

        for (int ball = 1; ball <= balls; ball++) {
            int bowlerRuns = bowler.bowl();
            int batsmanRuns = batsman.bat();

            if (!bowler.canTakeWicket() && batsmanRuns == bowlerRuns)
                continue;

            if (bowler.canTakeWicket() && umpire.decideBatsmanOut(batsmanRuns, bowlerRuns, batsman.getDismissalStrategy()))
                break;

            totalScore += batsmanRuns;

            if (targetAchieved())
                break;
        }
    }

    public boolean targetAchieved() {
        return totalScore >= targetScore;
    }

}
