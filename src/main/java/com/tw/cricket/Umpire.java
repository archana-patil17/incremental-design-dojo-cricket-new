package com.tw.cricket;

public class Umpire {
    public boolean decideBatsmanOut(int batsmanRuns, int bowlerRuns, DismissalStrategy dismissalStrategy) {
        return dismissalStrategy.decideOut(batsmanRuns, bowlerRuns);
    }
}
