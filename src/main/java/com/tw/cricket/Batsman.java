package com.tw.cricket;

public class Batsman {

    private final BatsmanBehaviour batsmanBehaviour;
    private final BatsmanType batsmanType;

    public Batsman(BatsmanBehaviour batsmanBehaviour, BatsmanType batsmanType) {
        this.batsmanBehaviour = batsmanBehaviour;
        this.batsmanType = batsmanType;
    }

    public int bat() {
        return batsmanBehaviour.bat();
    }

    public DismissalStrategy getDismissalStrategy() {
        return batsmanType.getDismissalStrategy();
    }
}
