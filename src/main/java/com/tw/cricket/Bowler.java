package com.tw.cricket;

import java.util.Random;

public class Bowler {

    private final BowlerType bowlerType;

    public Bowler(BowlerType bowlerType) {
        this.bowlerType = bowlerType;
    }

    public boolean canTakeWicket() {
        return this.bowlerType == BowlerType.NORMAL;
    }

    public int bowl() {
        Random random = new Random();
        int MAX_BOUND_VALUE = 7;
        return random.nextInt(MAX_BOUND_VALUE);
    }
}

/*
* 1. ENUMS -> BOWLER_TYPE
*   1. BOWLER TO HAVE A BOWLER_TYPE AND BOWLER TO DECIDE IF A PARTICULAR TYPE CAN TAKE WICKET OR NOR
*   2. INTRODUCE CAN_TAKE_WICKETS AS A BEHAVIOUR TO ENUM
*
*
* 2. INTERFACE - I_BOWLER (DISCUSSION POINT)
* TALKING ABOUT BOWL AS A DEFAULT METHOD, CAN_TAKE_WICKET CAN BE IMPLEMENTED IN THE CONCRETE CLASSES
*
*
* 3. STRATEGY
* BOWLER HAS A WICKET_TAKING_STRATEGY IN THE SENSE THAT IF THEY CAN TAKE WICKET OR NOT
*
*
* 4. BOWLER AS AN ABSTRACT CLASS
* BOWL WILL BE IMPLEMENTED HERE - OTHER TWO CLASSES CAN EXTEND IT
*
*
* */
