package com.tw.cricket;

import java.util.Random;

public class HittingBatsmanBehaviour implements BatsmanBehaviour {
    @Override
    public int bat() {
        int[] possibleScore = {0, 4, 6};
        int rnd = new Random().nextInt(possibleScore.length);
        return possibleScore[rnd];
    }
}
