package com.tw.cricket;

public class TailEnderBatsmanDismissalStrategy implements DismissalStrategy{
    @Override
    public boolean decideOut(int batsManScore, int bowlerScore) {
        return ((batsManScore == bowlerScore) || (batsManScore % 2 == bowlerScore % 2));
    }
}
