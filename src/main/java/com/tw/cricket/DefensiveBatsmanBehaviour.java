package com.tw.cricket;

import java.util.Random;

public class DefensiveBatsmanBehaviour implements BatsmanBehaviour {
    @Override
    public int bat() {
        int[] possibleScore = {0, 1, 2, 3};
        int rnd = new Random().nextInt(possibleScore.length);
        return possibleScore[rnd];
    }
}
