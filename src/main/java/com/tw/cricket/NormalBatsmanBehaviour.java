package com.tw.cricket;

import java.util.Random;

public class NormalBatsmanBehaviour implements BatsmanBehaviour {
    @Override
    public int bat() {
        Random random = new Random();
        int MAX_BOUND_VALUE = 7;
        return random.nextInt(MAX_BOUND_VALUE);
    }
}
