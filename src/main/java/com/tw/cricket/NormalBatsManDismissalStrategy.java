package com.tw.cricket;

public class NormalBatsManDismissalStrategy implements DismissalStrategy{
    @Override
    public boolean decideOut(int batsManScore, int bowlerScore) {
        return batsManScore == bowlerScore;
    }
}
