package com.tw.cricket;

public interface DismissalStrategy {
    public boolean decideOut(int batsManScore, int bowlerScore);
}
