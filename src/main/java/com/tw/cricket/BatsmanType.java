package com.tw.cricket;

public enum BatsmanType {
    NORMAL(new NormalBatsManDismissalStrategy()),
    HIT_MAN(new NormalBatsManDismissalStrategy()),
    DEFENSIVE(new NormalBatsManDismissalStrategy()),
    TAIL_ENDER(new TailEnderBatsmanDismissalStrategy());

    private DismissalStrategy dismissalStrategy;

    BatsmanType(DismissalStrategy dismissalStrategy) {
        this.dismissalStrategy = dismissalStrategy;
    }

    public DismissalStrategy getDismissalStrategy() {
        return dismissalStrategy;
    }
}
